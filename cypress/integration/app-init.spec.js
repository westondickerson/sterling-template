describe('Initialization', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('App Loads', () => {
    cy.get('[data-cy=app]').should('be.visible');
  });
});
