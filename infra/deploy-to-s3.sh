#!/bin/bash
if [ "$CODEBUILD_BUILD_SUCCEEDING" = 1 ]
then
    echo "Build succeeding, deploying built project to S3"
    aws s3 cp ./build s3://$DEPLOY_BUCKET_NAME/ --acl public-read --recursive
else
    echo "Build failing, not deploying to S3"
fi