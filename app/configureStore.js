import { createStore, applyMiddleware, compose } from 'redux';
import { fromJS, Iterable } from 'immutable';
import { routerMiddleware } from 'react-router-redux';
import createSagaMiddleware from 'redux-saga';
import { createLogger } from 'redux-logger';

import createReducer from './reducers';

const sagaMiddleware = createSagaMiddleware();
const logger = createLogger({
  // predicate: (getState, action) => action.type !== '@@router/LOCATION_CHANGE', //  Add actions here to hide from logger
  stateTransformer: state => {
    if (Iterable.isIterable(state)) return state.toJS();
    return state;
  },
  collapsed: true,
});

export default function configureStore(initialState = {}, history) {
  const middlewares = [sagaMiddleware, routerMiddleware(history), logger];

  const enhancers = [applyMiddleware(...middlewares)];

  /* eslint-disable no-underscore-dangle, indent */
  const composeEnhancers =
    process.env.NODE_ENV !== 'production' &&
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
      ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
          shouldHotReload: false,
        })
      : compose;

  const store = createStore(
    createReducer(),
    fromJS(initialState),
    composeEnhancers(...enhancers),
  );

  store.runSaga = sagaMiddleware.run;
  store.injectedReducers = {};
  store.injectedSagas = {};

  return store;
}
