/*
 *
 * Test saga
 *
 */

import { all, put, call, takeLatest } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { DEFAULT_ACTION } from './constants';
import { successDefaultAction } from './actions';

export function* changeValueSaga({ payload }) {
  try {
    const result = yield call(data => data, payload);
    yield call(delay, 2000); // 2 sec delay
    yield put(successDefaultAction(result));
  } catch (err) {
    // catch errors
    yield put(DEFAULT_ACTION.FAILURE, { payload: err });
  }
}

export default function* root() {
  yield all([takeLatest(DEFAULT_ACTION.REQUEST, changeValueSaga)]);
}
