/*
 *
 * Test constants
 *
 */

import { createConstants } from '../../utils/constants';

export const DEFAULT_ACTION = createConstants('Test/DEFAULT_ACTION');
