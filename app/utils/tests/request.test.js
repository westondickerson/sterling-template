import request from '../request';

global.fetch = require('jest-fetch-mock');

describe('Request util', () => {
  it('should parse JSON on success', () => {
    fetch.mockResponses([JSON.stringify({ data: 'foo' }), { status: 200 }]);
    return request('bar').then(response => {
      expect(response.data).toBe('foo');
    });
  });

  it('should give expected error message on error', () => {
    fetch.mockResponses([
      JSON.stringify({ statusText: 'Expected error' }),
      { status: 500 },
    ]);
    return request('bar').then(
      response => {
        expect(response.data).toBe('foo');
      },
      error => {
        error.response
          .text()
          .then(text =>
            expect(JSON.parse(text).statusText).toBe('Expected error'),
          );
      },
    );
  });

  it('should handle 204 correctly', () => {
    fetch.mockResponses([
      JSON.stringify({ statusText: 'Expected 204 value' }),
      { status: 204 },
    ]);
    return request('bar').then(response => {
      expect(response).toBeNull();
    });
  });

  it('should handle 205 correctly', () => {
    fetch.mockResponses([
      JSON.stringify({ statusText: 'Expected 205 value' }),
      { status: 205 },
    ]);
    request('bar');
  });
});
