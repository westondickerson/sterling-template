/**
 * Test helper functions
 */

import { createReducer } from '../helpers';

describe('createReducer', () => {
  // let store;

  beforeEach(() => {
    // store = configureStore({}, memoryHistory);
  });

  it('should generate a reducer', () => {
    const reducer = createReducer(
      { data: [] },
      {
        addData: (state, { payload }) => ({
          ...state,
          data: [...state.data, payload],
        }),
      },
    );
    expect(reducer).toBeDefined();
    let firstState = {};
    let secondState = {};
    firstState = reducer(undefined, { type: 'addData', payload: 'foo' });
    secondState = reducer(firstState, { type: 'addData', payload: 'bar' });
    expect(firstState).toEqual({ data: ['foo'] });
    expect(secondState).toEqual({ data: ['foo', 'bar'] });
    secondState = reducer(secondState, {
      type: 'removeData',
      payload: 'foobar',
    });
    expect(secondState).toEqual({ data: ['foo', 'bar'] });
  });
});
