import { createConstants } from '../constants';

describe('Util Constants', () => {
  it('should contain the expected constants', () => {
    const constants = createConstants('unitTest');
    expect(typeof constants.REQUEST !== 'undefined').toEqual(true);
    expect(typeof constants.SUCCESS !== 'undefined').toEqual(true);
    expect(typeof constants.FAILURE !== 'undefined').toEqual(true);
  });
});
